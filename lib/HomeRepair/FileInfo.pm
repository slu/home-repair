package HomeRepair::FileInfo;

use v5.26;
use feature qw(signatures);
no warnings qw(experimental::signatures);

use File::Basename qw(basename dirname);
use File::Find;
use Time::HiRes qw(gettimeofday tv_interval);

use HomeRepair::StringUtils qw(shrink);

sub new($class, $root_dir, $term) {
    my %files;
    my %elapsed;
    my $self = { root_dir => $root_dir, term => $term, files => \%files };
    bless $self, $class;
}


sub update($self) {
    my $t0 = [gettimeofday];

    find({ wanted => sub {
               state $file_count++;
               printf "\033[u\033[K%5d %s", $file_count, shrink($_, $self->{term}->columns()-7);

               my $parent = $_ ne $self->{root_dir} ? dirname($_) : undef;
               my %info = ( parent => $parent );

               push @{$self->{files}{$parent}{children}}, $_ if $parent;

               if (-f $_) {
                   %info = (%info, ( type => 'F', size => -s $_ ));
               } elsif (-d $_) {
                   %info = (%info, (type => 'D', children => [] ));
               } else {
                   %info = (%info, (type => 'O' ));
               }

               $self->{files}{$_} = \%info;
           }, no_chdir => 1 }, $self->{root_dir});

    return tv_interval($t0);
}

sub count($self) {
    return scalar %{$self->{files}};
}

sub stats($self) {
    my $t0 = [gettimeofday];
    my $size = 0;
    my $count = { F => 0, D => 0, O => 0 };

    for my $path (keys %{$self->{files}}) {
        state $file_count++;
        printf "\033[u\033[K%5d", $file_count;
        $count->{$self->{files}->{$path}->{type}}++;
        $size += $self->{files}->{$path}->{size} if $self->{files}->{$path}->{type} eq 'F';
    }
    return { size => $size, count => $count, elapsed => tv_interval($t0) };
}

1;
