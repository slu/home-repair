package HomeRepair::Terminal;

use v5.26;
use feature qw(signatures);
no warnings qw(experimental::signatures);

use Term::Cap;


sub new($class) {
    my $columns = `tput cols` // 80;
    my $terminal = Term::Cap->Tgetent();
    my $clear = $terminal->Tputs('cl');
    my $self = { columns => $columns, terminal => $terminal, clear => $clear };
    bless $self, $class;
}

sub columns($self) {
    return $self->{columns};
}


sub clear($self) {
    print $self->{clear};
}

1;
