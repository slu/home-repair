package HomeRepair::StringUtils;

use v5.26;
use feature qw(signatures);
no warnings qw(experimental::signatures);

use POSIX qw(ceil floor);

use base 'Exporter';
our @EXPORT_OK = qw(human_readable shrink);


sub human_readable($size_in_bytes) {
    my $exp = 0;

    state $units = [qw(B KB MB GB TB PB)];

    for (@$units) {
        last if $size_in_bytes < 1024;
        $size_in_bytes /= 1024;
        $exp++;
    }

    return sprintf("%.2f %s", $size_in_bytes, $units->[$exp]);
}


sub shrink($str, $width) {
    my $len = length($str);
    if ($len > $width) { # 15 > 10
        my $cut = $len - $width; # 15 - 10 = 5
        $str = substr($str, 0, ceil($len/2-$cut/2)-1) . '..' . substr($str, -floor($len/2-$cut/2)+1);
    }
    return $str;
}

1;
