#!/usr/bin/env perl

use v5.26;
use feature qw(signatures);
no warnings qw(experimental::signatures);

use Cwd qw(abs_path);
use Data::Dumper;
use File::Find;
use File::Spec;
use FindBin;
use Time::HiRes qw(gettimeofday tv_interval);

use lib File::Spec->catdir($FindBin::Bin, '..', 'lib');

use HomeRepair::FileInfo;
use HomeRepair::StringUtils qw(human_readable);
use HomeRepair::Terminal;

$| = 1;


sub root_dir() {
    my $root_dir = '.';

    if (exists $ENV{HOME}) {
        $root_dir = $ENV{HOME};
    } elsif (exists $ENV{HOMEDRIVE} && exists $ENV{HOMEPATH}) {
        $root_dir = $ENV{HOMEDRIVE} . $ENV{HOMEPATH};
    }

    return abs_path($root_dir);
}


sub main() {
    my $term = HomeRepair::Terminal->new();

    $term->clear();
    say "HOME REPAIR";
    say "===========\n";

    print "\033[s\n\n";

    my $root_dir = root_dir();

    printf "%-20s: '%s'\n", 'Root directory', $root_dir;

    my $file_info = HomeRepair::FileInfo->new($root_dir, $term);
    my $elapsed = $file_info->update();

    printf "\033[u\033[K\n\n\n";

    printf "%-20s: %d\n", 'Path count', $file_info->count();

    my $stats = $file_info->stats();

    printf "\033[u\033[K\n\n\n\n";

    printf "%-20s: %d\n", 'Directory count', $stats->{count}{D};
    printf "%-20s: %d\n", 'Other count', $stats->{count}{O};
    printf "%-20s: %d\n", 'File count', $stats->{count}{F};
    printf "%-20s: %s\n", 'File size', human_readable($stats->{size});

    say "\nElapsed times in seconds:\n";
    printf "%-20s: %.2f\n", 'Traversing filesystem', $elapsed;
    printf "%-20s: %.2f\n", 'Generating statistics', $stats->{elapsed};
}

main();

# TODO: Check for git repos


#sub hier($path, $level) {
#    my $name = basename($path);
#    print '  'x$level;
#    if ($paths{$path}->{type} eq 'F') {
#        say "$name";
#    } elsif ($paths{$path}->{type} eq 'D') {
#        say "[$name]";
#        for my $child (sort @{$paths{$path}->{children}}) {
#            hier($child, $level+1);
#        }
#    } else {
#        say "<$name>";
#    }
#}
#
#hier($root_dir, 0);
